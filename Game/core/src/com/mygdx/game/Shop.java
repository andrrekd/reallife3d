/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author randr
 */
public class Shop extends Building {
    public final static int SKINPRICE1 = 20;
    public final static int SKINPRICE2 = 40;
    public final static int SKINPRICE3 = 60;
    public Shop(int xPos, int yPos, Texture texture, float size) {
        super(xPos, yPos, texture, size);
    }
    public Item buyCloth() {
        return new Item("Red square", "The red square representing the player");
    }
    public Item buyRandomItemThatDoesentShow() {
        return new Item("Its here", "Random item in your inventory");
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author randr
 */
public class House extends Building {
    private Inventory playerInventory;
    public House(int xPos, int yPos, Texture texture, float size, Inventory playerInventory) {
        super(xPos, yPos, texture, size);
        this.playerInventory = playerInventory;
    }
    public Inventory getInventory(){
        return this.playerInventory;
    }
    public void updateInventory(Inventory inventory){
        this.playerInventory = inventory;
    }
    
    
}

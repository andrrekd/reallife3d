/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Andreas
 */
public class Inventory {
    private ArrayList<Item> inventory;

    public Inventory() {
        inventory = new ArrayList<Item>();
    }
    
    public boolean addItem(Item item) {
        for (Item currentItem : inventory) {
            if (currentItem.getName().equals(item.getName())){
                return false;
            }
        }
        return inventory.add(item);
    }
    
    public boolean removeItem(Item item) {
        for (Item currentItem : inventory) {
            if (currentItem == item){
                inventory.remove(currentItem);
                return true;
            }
        }
        return true;
    }
    public ArrayList<Item> getItems(){
        return this.inventory;
    }
    public boolean contains(String itemName){
        for (Item item : inventory) {
            if (item.getName().equals(itemName)) return true;
        }
        return false;
    }
}

package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.sidescroller.screens.SidescrollerScreen;
import com.mygdx.sidescroller.stages.GameStage;

/**
 * This is the first class called from the Launcher class (main class) Central
 * class for the game initializes the stage
 *
 * @version 0.1
 * @author Andreas
 */
public class RealLife3D extends Game {

    private SpriteBatch batch;

    //The screen in which the game runs, this contains the logic for the game
    private GameScreen gameScreen;

    //Width of the screen
    public static final int WIDTH = 800;
    //Height of the screen
    public static final int HEIGHT = 600;

    private StartScreen startScreen;

    private MenuScreen menuScreen;

    private SidescrollerScreen sidescrollerScreen;

    private GameStage obstacleCourse;
    private Music worldMusic;
    private Music obstacleMusic;
    private Sound startUpSound;

    public RealLife3D() {

    }

    /**
     * First called when the application is started
     */
    @Override
    public void create() {

        batch = new SpriteBatch();
        gameScreen = new GameScreen(batch, WIDTH, HEIGHT);
        menuScreen = new MenuScreen();
        startScreen = new StartScreen();
        sidescrollerScreen = new SidescrollerScreen();
        obstacleCourse = new GameStage();

        super.setScreen(startScreen);
        
        startUpSound = Gdx.audio.newSound(Gdx.files.internal("startupsound.ogg"));
        worldMusic = Gdx.audio.newMusic(Gdx.files.internal("worldmusic.mp3"));
        obstacleMusic = Gdx.audio.newMusic(Gdx.files.internal("obstaclemusic.wav"));
        obstacleMusic.setVolume(0.1f);
        worldMusic.setVolume(0.2f);
        
        startUpSound.play();
    }

    /**
     * Updates the screen called for each frame
     */
    @Override
    public void render() {
        super.render();
        
        if (startScreen.hasStarted() == true) {
            super.setScreen(gameScreen);
            obstacleMusic.pause();
            if (!worldMusic.isPlaying()) {
                worldMusic.play();
            }
        }

        if (gameScreen.GoToMenu() == true) {
            super.setScreen(menuScreen);
        }

        if (menuScreen.isContinued() == true) {
            super.setScreen(gameScreen);
            gameScreen.setGoToMenuFalse();
            menuScreen.setFalse();
        }

        if (gameScreen.GoToSideScrollingGame() == true) {
            super.setScreen(sidescrollerScreen);
            worldMusic.pause();
            if (!obstacleMusic.isPlaying()) {
                obstacleMusic.play();
            }
        }

        if (sidescrollerScreen.goToGameScreen() == true) {
            gameScreen.getPlayer().addMoney(sidescrollerScreen.getStage().getNumberOfObstacles());
            sidescrollerScreen.getStage().setNumberOfObstaclesToZero();

            super.setScreen(gameScreen);
            //sidescrollerScreen.stage.removeObsatcle();
            sidescrollerScreen.getStage().setUpRunner();
            sidescrollerScreen.setGoToGameScreenFalse();
            gameScreen.setGoToSideScrollingGameFalse();
            sidescrollerScreen.getStage().removeObsatcle();
        }

    }

    /**
     * Called when the application ends
     */
    @Override
    public void dispose() {
        batch.dispose();
    }

}

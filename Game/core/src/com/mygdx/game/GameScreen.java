/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import com.mygdx.sidescroller.utils.Constants;

/**
 * The screen in which the game runs Handles the logic for the game
 *
 * @author Andreas
 * @version 0.1
 */
public class GameScreen implements Screen {

    private final Player player;
    private final Shop shop;
    private final House house;
    private final Stable stable;
    private final Sprite shopSprite;
    private final Sprite playerSprite;
    private final Sprite houseSprite;
    private final Sprite stableSprite;
    private final Sprite signSprite;
    private Rectangle shopRectangle;
    private Rectangle playerRectangle;
    private Rectangle houseRectangle;
    private Rectangle stableRectangle;

    private final Batch batch;
    private final int WIDTH;
    private final int HEIGHT;
    private final Texture backgroundTexture;
    private OrthographicCamera camera;
    private final int LEFT = 0;
    private final int RIGHT = 1;
    private final int UP = 2;
    private final int DOWN = 3;
    private final int NONE = 4;
    private int direction = DOWN;
    private int illegalDirection;

    private Skin skin;
    private BitmapFont bitmapFont;
    private Pixmap pixmap;
    private final Stage shopStage;
    private final Stage houseStage;
    private final Stage gameStage;
    private final Stage stableStage;
    private TextButton shopButton1;
    private TextButton shopButton2;
    private TextButton shopButton3;
    private TextButton shopButton4;
    private TextButton shopButton5;
    private TextButton shopButton6;
    private boolean buttonIsClicked = false;
    private boolean buttonIsClicked2 = false;
    private boolean buttonIsClicked3 = false;
    private boolean buttonIsClicked4 = false;
    private boolean buttonIsClicked5 = false;
    private boolean buttonIsClicked6 = false;
    private final Sound coinSound;
    private final Sound errorSound;
    private final Sound buttonSound;
    //private final Music music;

    private boolean shopIsDrawn = false;
    private boolean houseIsDrawn = false;
    private boolean stablesIsDrawn = false;
    private boolean goToMenu;
    private boolean goToSideScroller;
    private final Stage obsacleStage;
    private TextButton obstacleCourseButton;
    private TextButton houseButton1;
    private TextButton houseButton2;
    private TextButton houseButton3;
    private TextButton houseButton4;
    private TextButton houseButton5;
    private TextButton stableButton;
    private TextButton stableButton2;
    private TextButton stableButton3;
    private final TextButton moneyUI;

    private float elapsedTime = 0;
    private Animation currentAnimation;

    private TextureAtlas defaultDownTextureAtlas;
    private TextureAtlas defaultUPTexttureAtlas;
    private TextureAtlas defaultLeftTextureAtlas;
    private TextureAtlas defaultRightTextureAtlas;
    private TextureAtlas defaultStillTextureAtlas;
    private final Animation defaultWalkDown;
    private final Animation defaultWalkUp;
    private final Animation defaultWalkLeft;
    private final Animation defaultWalkRight;
    private final Animation defaultWalkStill;

    private TextureAtlas flowerdressDownTextureAtlas;
    private TextureAtlas flowerdressUPTexttureAtlas;
    private TextureAtlas flowerdressLeftTextureAtlas;
    private TextureAtlas flowerdressRightTextureAtlas;
    private TextureAtlas flowerdressStillTextureAtlas;
    private final Animation flowerdressWalkDown;
    private final Animation flowerdressWalkUp;
    private final Animation flowerdressWalkLeft;
    private final Animation flowerdressWalkRight;
    private final Animation flowerdressWalkStill;

    private TextureAtlas pinkdressDownTextureAtlas;
    private TextureAtlas pinkdressUPTexttureAtlas;
    private TextureAtlas pinkdressLeftTextureAtlas;
    private TextureAtlas pinkdressRightTextureAtlas;
    private TextureAtlas pinkdressStillTextureAtlas;
    private final Animation pinkdressWalkDown;
    private final Animation pinkdressWalkUp;
    private final Animation pinkdressWalkLeft;
    private final Animation pinkdressWalkRight;
    private final Animation pinkdressWalkStill;

    private TextureAtlas purplebrownDownTextureAtlas;
    private TextureAtlas purplebrownUPTexttureAtlas;
    private TextureAtlas purplebrownLeftTextureAtlas;
    private TextureAtlas purplebrownRightTextureAtlas;
    private TextureAtlas purplebrownStillTextureAtlas;
    private final Animation purplebrownWalkDown;
    private final Animation purplebrownWalkUp;
    private final Animation purplebrownWalkLeft;
    private final Animation purplebrownWalkRight;
    private final Animation purplebrownWalkStill;

    private TextureAtlas redblueDownTextureAtlas;
    private TextureAtlas redblueUPTexttureAtlas;
    private TextureAtlas redblueLeftTextureAtlas;
    private TextureAtlas redblueRightTextureAtlas;
    private TextureAtlas redblueStillTextureAtlas;
    private final Animation redblueWalkDown;
    private final Animation redblueWalkUp;
    private final Animation redblueWalkLeft;
    private final Animation redblueWalkRight;
    private final Animation redblueWalkStill;

    /**
     * Constructor for the screen to run the game
     *
     * @param batch the batch the game is running in
     * @param WIHTH the height of the screen
     * @param HEIGHT the width of the screen
     */
    public GameScreen(Batch batch, int WIHTH, int HEIGHT) {
        this.WIDTH = WIHTH;
        this.HEIGHT = HEIGHT;
        this.batch = batch;
        backgroundTexture = new Texture(Gdx.files.internal("field.png"));
        coinSound = Gdx.audio.newSound(Gdx.files.internal("coins.wav"));
        errorSound = Gdx.audio.newSound(Gdx.files.internal("error.wav"));
        buttonSound = Gdx.audio.newSound(Gdx.files.internal("button.wav"));
        long id = buttonSound.play(0.1f);
        buttonSound.setVolume(id, 0.1f);
        //music = Gdx.audio.newMusic(Gdx.files.internal("worldmusic.mp3"));
        goToMenu = false;
        goToSideScroller = false;

        player = new Player(250, 250, new Texture(Gdx.files.internal("trex.png")), 100);
        playerSprite = new Sprite(player.getTexture());
        playerRectangle = new Rectangle(player.getxPos(), player.getyPos(),
                player.getSize(), player.getSize());

        shop = new Shop(0, 350, new Texture(Gdx.files.internal("shop.png")), 125);
        shopSprite = new Sprite(shop.getTexture());
        shopRectangle = new Rectangle(shop.getxPos(), (shop.getyPos()+50),
                shop.getSize() * 2, shop.getSize()-80);

        house = new House(525, 350, new Texture(Gdx.files.internal("house.png")), 150, player.getInventory());
        houseSprite = new Sprite(house.getTexture());
        houseRectangle = new Rectangle(house.getxPos()+50, house.getyPos()+50,
                ((house.getSize() * (1.25f))-70), (house.getSize())-60);

        stable = new Stable(0, 75, new Texture(Gdx.files.internal("stable.png")), 200);
        stableSprite = new Sprite(stable.getTexture());
        stableRectangle = new Rectangle(stable.getxPos(), stable.getyPos(), stable.getSize()*1.25f, stable.getSize()-100);
        
        signSprite = new Sprite(new Texture(Gdx.files.internal("sign.png")));
        
        camera = new OrthographicCamera();
        camera.setToOrtho(false, WIHTH, HEIGHT);

        shopStage = new Stage();
        houseStage = new Stage();
        stableStage = new Stage();
        obsacleStage = new Stage();
        gameStage = new Stage();
        createBasicSkin();

        shopButton1 = new TextButton("Buy a dress with flowers on, 20HC", skin); // Use the initialized skin
        shopButton2 = new TextButton("Buy a pink dress, 20HC", skin); // Use the initialized skin
        shopButton3 = new TextButton("Buy a purple and brown skin, 40HC", skin); // Use the initialized skin
        shopButton4 = new TextButton("Buy a red and blue skin, 40HC", skin); // Use the initialized skin
        shopButton5 = new TextButton("Buy tjukken, 40HC", skin); // Use the initialized skin
        shopButton6 = new TextButton("Buy the superhorse, 60HC", skin); // Use the initialized skin

        houseButton1 = new TextButton("Wear the default skin", skin); // Use the initialized skin
        houseButton2 = new TextButton("Wear dress with flowers on", skin); // Use the initialized skin
        houseButton3 = new TextButton("Wear a pink dress", skin); // Use the initialized skin
        houseButton4 = new TextButton("Wear a purple and brown skin", skin); // Use the initialized skin
        houseButton5 = new TextButton("Wear a red and blue skin", skin); // Use the initialized skin

        stableButton = new TextButton("Use Svein the horse", skin);
        stableButton2 = new TextButton("Use Tjukken", skin);
        stableButton3 = new TextButton("Use Superhorse", skin);

        obstacleCourseButton = new TextButton("Go to obstacle course", skin); // Use the initialized skin
        moneyUI = new TextButton("HorseCoin: " + player.getMoney(), creatMoneySkin()); // Use the initialized skin
        obstacleCourseButton = new TextButton("Go to obstacle course", skin); // Use the initialized skin

        defaultDownTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Default/front.txt"));
        defaultUPTexttureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Default/back.txt"));
        defaultLeftTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Default/left.txt"));
        defaultRightTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Default/right.txt"));
        defaultStillTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Default/still.txt"));
        defaultWalkDown = new Animation(0.1f, (defaultDownTextureAtlas.getRegions()));
        defaultWalkUp = new Animation(0.1f, (defaultUPTexttureAtlas.getRegions()));
        defaultWalkLeft = new Animation(0.1f, (defaultLeftTextureAtlas.getRegions()));
        defaultWalkRight = new Animation(0.1f, (defaultRightTextureAtlas.getRegions()));
        defaultWalkStill = new Animation(0.1f, (defaultStillTextureAtlas.getRegions()));
        currentAnimation = defaultWalkStill;

        flowerdressDownTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Flowerdress/front.txt"));
        flowerdressUPTexttureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Flowerdress/back.txt"));
        flowerdressLeftTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Flowerdress/left.txt"));
        flowerdressRightTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Flowerdress/right.txt"));
        flowerdressStillTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Flowerdress/still.txt"));
        flowerdressWalkDown = new Animation(0.1f, (flowerdressDownTextureAtlas.getRegions()));
        flowerdressWalkUp = new Animation(0.1f, (flowerdressUPTexttureAtlas.getRegions()));
        flowerdressWalkLeft = new Animation(0.1f, (flowerdressLeftTextureAtlas.getRegions()));
        flowerdressWalkRight = new Animation(0.1f, (flowerdressRightTextureAtlas.getRegions()));
        flowerdressWalkStill = new Animation(0.1f, (flowerdressStillTextureAtlas.getRegions()));

        pinkdressDownTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Pinkdress/front.txt"));
        pinkdressUPTexttureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Pinkdress/back.txt"));
        pinkdressLeftTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Pinkdress/left.txt"));
        pinkdressRightTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Pinkdress/right.txt"));
        pinkdressStillTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Pinkdress/still.txt"));
        pinkdressWalkDown = new Animation(0.1f, (pinkdressDownTextureAtlas.getRegions()));
        pinkdressWalkUp = new Animation(0.1f, (pinkdressUPTexttureAtlas.getRegions()));
        pinkdressWalkLeft = new Animation(0.1f, (pinkdressLeftTextureAtlas.getRegions()));
        pinkdressWalkRight = new Animation(0.1f, (pinkdressRightTextureAtlas.getRegions()));
        pinkdressWalkStill = new Animation(0.1f, (pinkdressStillTextureAtlas.getRegions()));

        purplebrownDownTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Purplebrown/front.txt"));
        purplebrownUPTexttureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Purplebrown/back.txt"));
        purplebrownLeftTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Purplebrown/left.txt"));
        purplebrownRightTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Purplebrown/right.txt"));
        purplebrownStillTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Purplebrown/still.txt"));
        purplebrownWalkDown = new Animation(0.1f, (purplebrownDownTextureAtlas.getRegions()));
        purplebrownWalkUp = new Animation(0.1f, (purplebrownUPTexttureAtlas.getRegions()));
        purplebrownWalkLeft = new Animation(0.1f, (purplebrownLeftTextureAtlas.getRegions()));
        purplebrownWalkRight = new Animation(0.1f, (purplebrownRightTextureAtlas.getRegions()));
        purplebrownWalkStill = new Animation(0.1f, (purplebrownStillTextureAtlas.getRegions()));

        redblueDownTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Redblue/front.txt"));
        redblueUPTexttureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Redblue/back.txt"));
        redblueLeftTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Redblue/left.txt"));
        redblueRightTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Redblue/right.txt"));
        redblueStillTextureAtlas = new TextureAtlas(Gdx.files.internal("Skins/Redblue/still.txt"));
        redblueWalkDown = new Animation(0.1f, (redblueDownTextureAtlas.getRegions()));
        redblueWalkUp = new Animation(0.1f, (redblueUPTexttureAtlas.getRegions()));
        redblueWalkLeft = new Animation(0.1f, (redblueLeftTextureAtlas.getRegions()));
        redblueWalkRight = new Animation(0.1f, (redblueRightTextureAtlas.getRegions()));
        redblueWalkStill = new Animation(0.1f, (redblueStillTextureAtlas.getRegions()));

    }

    /**
     * Called when the screen comes out from hiding
     */
    @Override
    public void show() {
        obstacleCourseButton.setPosition(Gdx.graphics.getWidth() / 2 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - Gdx.graphics.getHeight() / 4));
        obsacleStage.addActor(obstacleCourseButton);
        moneyUI.setPosition(Gdx.graphics.getWidth() - moneyUI.getWidth(), Gdx.graphics.getHeight() - moneyUI.getHeight());
        shopStage.act();
        houseStage.act();
        stableStage.act();
        obsacleStage.act();
        gameStage.act();

    }

    /**
     * The gameloop, this happens every frame
     *
     * @param delta
     */
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(backgroundTexture, 0, 0);
        shopSprite.setPosition(shop.getxPos(), shop.getyPos());
        shopSprite.setSize(shop.getSize() * 2, shop.getSize());
        shopSprite.draw(batch);
        //#TODO: fix different sizes
        houseSprite.setPosition(house.getxPos(), house.getyPos());
        houseSprite.setSize(house.getSize() * (1.25f), house.getSize());
        houseSprite.draw(batch);

        stableSprite.setPosition(stable.getxPos(), stable.getyPos());
        stableSprite.setSize(stable.getSize()*1.25f, stable.getSize());
        stableSprite.draw(batch);
        
        signSprite.setPosition(110, 400);
        signSprite.draw(batch);
        
        currentAnimation = currentAnimation();
        elapsedTime += Gdx.graphics.getDeltaTime();
        batch.draw((TextureRegion) currentAnimation.getKeyFrame(elapsedTime, true), player.getxPos(), player.getyPos(), player.getVarSize(), player.getVarSize());

        batch.end();
        PleaseKeepPlayerInTheMap();
        player.calculatePlayerSize();
        gameStage.addActor(moneyUI);
        moneyUI.setText("HorseCoin: " + player.getMoney());
        gameStage.draw();

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && (illegalDirection != LEFT)) {
            direction = LEFT;
            player.movePlayerLeft();
        } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && (illegalDirection != RIGHT)) {
            direction = RIGHT;
            player.movePlayerRight();
        } else if (Gdx.input.isKeyPressed(Input.Keys.DOWN) && (illegalDirection != DOWN)) {
            direction = DOWN;
            player.movePlayerDown();
        } else if (Gdx.input.isKeyPressed(Input.Keys.UP) && (illegalDirection != UP)) {
            direction = UP;
            player.movePlayerUp();
        } else if (Gdx.input.isKeyPressed(Input.Keys.A) && (illegalDirection != LEFT)) {
            direction = LEFT;
            player.movePlayerLeft();
        } else if (Gdx.input.isKeyPressed(Input.Keys.D) && (illegalDirection != RIGHT)) {
            direction = RIGHT;
            player.movePlayerRight();
        } else if (Gdx.input.isKeyPressed(Input.Keys.S) && (illegalDirection != DOWN)) {
            direction = DOWN;
            player.movePlayerDown();
        } else if (Gdx.input.isKeyPressed(Input.Keys.W) && (illegalDirection != UP)) {
            direction = UP;
            player.movePlayerUp();
        } else {
            direction = NONE;
        }

        playerSprite.setPosition(player.getxPos(), player.getyPos());
        playerSprite.setSize(player.calculatePlayerSize(), player.calculatePlayerSize());
        playerRectangle.setPosition(player.getxPos(), player.getyPos());
        if ((Intersector.overlaps(playerRectangle, shopRectangle)) 
                || (Intersector.overlaps(playerRectangle, houseRectangle)) 
                || (Intersector.overlaps(playerRectangle, stableRectangle))) {

            switch (direction) {
                case LEFT:
                    illegalDirection = LEFT;
                    break;
                case RIGHT:
                    illegalDirection = RIGHT;
                    break;
                case DOWN:
                    illegalDirection = DOWN;
                    break;
                case UP:
                    illegalDirection = UP;
                    break;

            }
            if (Intersector.overlaps(playerRectangle, shopRectangle)) {
                this.shopIsDrawn = true;
                shopStage.draw();
            } else {
                this.shopIsDrawn = false;
            }
            if (Intersector.overlaps(playerRectangle, houseRectangle)) {
                this.houseIsDrawn = true;
                houseStage.draw();
            } else {
                this.houseIsDrawn = false;
            }
            if (Intersector.overlaps(playerRectangle, stableRectangle)) {
                this.stablesIsDrawn = true;
                stableStage.draw();
            } else {
                this.stablesIsDrawn = false;
            }

        } else {
            illegalDirection = NONE;
            this.shopIsDrawn = false;
            this.houseIsDrawn = false;
            this.stablesIsDrawn = false;
        }

        if (shopIsDrawn) {
            Gdx.input.setInputProcessor(shopStage);// Make the stage consume events
            shopButton1.setPosition(Gdx.graphics.getWidth() / 3 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - 50));
            shopButton2.setPosition(Gdx.graphics.getWidth() / 3 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - 100));
            shopButton3.setPosition(Gdx.graphics.getWidth() / 3 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - 150));
            shopButton4.setPosition(Gdx.graphics.getWidth() / 3 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - 200));
            shopButton5.setPosition(Gdx.graphics.getWidth() / 3 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - 250));
            shopButton6.setPosition(Gdx.graphics.getWidth() / 3 - Gdx.graphics.getWidth() / 8, (Gdx.graphics.getHeight() - 300));
            shopStage.addActor(shopButton1);
            shopStage.addActor(shopButton2);
            shopStage.addActor(shopButton3);
            shopStage.addActor(shopButton4);
            shopStage.addActor(shopButton5);
            shopStage.addActor(shopButton6);
            shopStage.addActor(moneyUI);
            moneyUI.setTouchable(Touchable.disabled);

            shopButton1.addCaptureListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    shopButton1 = new TextButton("Buy a dress with flowers on, 20HC", skin); // Use the initialized skin
                    if ((player.getMoney() >= Shop.SKINPRICE1) && (!buttonIsClicked)
                            && (!player.getInventory().contains("flowerdress"))) {
                        buttonIsClicked = true;
                        coinSound.play();
                        player.removeMoney(shop.SKINPRICE1);
                        player.addItem(new Item("flowerdress", "A dress with flowers on"));
                    } else if (!buttonIsClicked && (player.getInventory().contains("flowerdress") == true)) {
                        shopButton1.setColor(Color.GOLD);
                        shopButton1.setText("already owned");
                        errorSound.play(0.1f);
                    } else if (!buttonIsClicked) {
                        shopButton1.setColor(Color.RED);
                        shopButton1.setText("Need more coins");
                        errorSound.play(0.1f);

                    }
                }
            }
            );
            shopButton2.addCaptureListener(
                    new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y
                ) {
                    shopButton2 = new TextButton("Buy a pink dress, 20HC", skin); // Use the initialized skin
                    if (player.getMoney() >= Shop.SKINPRICE1 && (!buttonIsClicked2)
                            && (!player.getInventory().contains("pinkdress"))) {
                        buttonIsClicked2 = true;
                        coinSound.play();
                        player.removeMoney(Shop.SKINPRICE1);
                        player.addItem(new Item("pinkdress", "A pink dress"));

                    } else if (!buttonIsClicked2 && (player.getInventory().contains("pinkdress") == true)) {
                        shopButton2.setColor(Color.GOLD);
                        shopButton2.setText("already owned");
                        errorSound.play(0.1f);
                    } else if (!buttonIsClicked2) {
                        shopButton2.setColor(Color.RED);
                        shopButton2.setText("Need more coins");
                        errorSound.play(0.1f);
                    }
                }
            }
            );
            shopButton3.addCaptureListener(
                    new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y
                ) {
                    shopButton3 = new TextButton("Buy a purple and brown skin, 40HC", skin); // Use the initialized skin
                    if (player.getMoney() >= Shop.SKINPRICE2 && (!buttonIsClicked3)
                            && (!player.getInventory().contains("purplebrown"))) {
                        buttonIsClicked3 = true;
                        coinSound.play();
                        player.removeMoney(Shop.SKINPRICE2);
                        player.addItem(new Item("purplebrown", "A purple and brown skin"));
                    } else if (!buttonIsClicked3 && (player.getInventory().contains("purplebrown") == true)) {
                        shopButton3.setColor(Color.GOLD);
                        shopButton3.setText("already owned");
                        errorSound.play(0.1f);
                    } else if (!buttonIsClicked3) {
                        shopButton3.setColor(Color.RED);
                        shopButton3.setText("Need more coins");
                        errorSound.play(0.1f);
                    }
                }
            }
            );
            shopButton4.addCaptureListener(
                    new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y
                ) {
                    shopButton4 = new TextButton("Buy a red and blue skin, 40HC", skin); // Use the initialized skin
                    if (player.getMoney() >= Shop.SKINPRICE2 && (!buttonIsClicked4)
                            && (!player.getInventory().contains("redblue"))) {
                        buttonIsClicked4 = true;
                        coinSound.play();
                        player.removeMoney(Shop.SKINPRICE2);
                        player.addItem(new Item("redblue", "A red and blue skin"));
                    } else if (!buttonIsClicked4 && (player.getInventory().contains("redblue") == true)) {
                        shopButton4.setColor(Color.GOLD);
                        shopButton4.setText("already owned");
                        errorSound.play(0.1f);
                    } else if (!buttonIsClicked4) {
                        shopButton4.setColor(Color.RED);
                        shopButton4.setText("Need more coins");
                        errorSound.play(0.1f);
                    }
                }
            }
            );
            shopButton5.addCaptureListener(
                    new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y
                ) {
                    shopButton5 = new TextButton("Buy tjukken, 40HC", skin); // Use the initialized skin
                    if (player.getMoney() >= Shop.SKINPRICE2 && (!buttonIsClicked5)
                            && (!player.getInventory().contains("tjukken"))) {
                        buttonIsClicked5 = true;
                        coinSound.play();
                        player.removeMoney(Shop.SKINPRICE2);
                        player.addItem(new Item("tjukken", "tjukken"));
                    } else if (!buttonIsClicked5 && (player.getInventory().contains("tjukken") == true)) {
                        shopButton5.setColor(Color.GOLD);
                        shopButton5.setText("already owned");
                        errorSound.play(0.1f);
                    } else if (!buttonIsClicked5) {
                        shopButton5.setColor(Color.RED);
                        shopButton5.setText("Need more coins");
                        errorSound.play(0.1f);
                    }
                }
            }
            );
            shopButton6.addCaptureListener(
                    new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y
                ) {
                    shopButton6 = new TextButton("Buy the superhorse, 60HC", skin); // Use the initialized skin
                    if (player.getMoney() >= Shop.SKINPRICE3 && (!buttonIsClicked6)
                            && (!player.getInventory().contains("superhorse"))) {
                        buttonIsClicked6 = true;
                        coinSound.play();
                        player.removeMoney(Shop.SKINPRICE3);
                        player.addItem(new Item("superhorse", "The superhorse"));
                    } else if (!buttonIsClicked6 && (player.getInventory().contains("superhorse") == true)) {
                        shopButton6.setColor(Color.GOLD);
                        shopButton6.setText("already owned");
                        errorSound.play(0.1f);
                    } else if (!buttonIsClicked6) {
                        shopButton6.setColor(Color.RED);
                        shopButton6.setText("Need more coins");
                        errorSound.play(0.1f);
                    }
                }
            }
            );
        } else {
            buttonIsClicked = false;
            buttonIsClicked2 = false;
            buttonIsClicked3 = false;
            buttonIsClicked4 = false;
            buttonIsClicked5 = false;
            buttonIsClicked6 = false;
            shopButton1 = new TextButton("Buy a dress with flowers on, 20HC", skin); // Use the initialized skin
            shopButton2 = new TextButton("Buy a pink dress, 20HC", skin); // Use the initialized skin
            shopButton3 = new TextButton("Buy a purple and brown skin, 40HC", skin); // Use the initialized skin
            shopButton4 = new TextButton("Buy a red and blue skin, 40HC", skin); // Use the initialized skin
            shopButton5 = new TextButton("Buy tjukken, 40HC", skin); // Use the initialized skin
            shopButton6 = new TextButton("Buy the superhorse, 60HC", skin); // Use the initialized skin
        }

        if (houseIsDrawn) {

            Gdx.input.setInputProcessor(houseStage);// Make the stage consume events
            houseButton1.setPosition(200, (Gdx.graphics.getHeight() - 50));
            houseButton2.setPosition(200, (Gdx.graphics.getHeight() - 100));
            houseButton3.setPosition(200, (Gdx.graphics.getHeight() - 150));
            houseButton4.setPosition(200, (Gdx.graphics.getHeight() - 200));
            houseButton5.setPosition(200, (Gdx.graphics.getHeight() - 250));
            houseStage.addActor(moneyUI);

            if (this.player.getInventory().contains("default")) {

                houseStage.addActor(houseButton1);
                houseButton1.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        player.setSkin("default");
                        houseButton1 = new TextButton("Wear the default skin", skin); // Use the initialized skin
                        buttonSound.play(0.1f);
                    }
                });
            }
            if (this.player.getInventory().contains("flowerdress")) {
                houseStage.addActor(houseButton2);
                houseButton2.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        player.setSkin("flowerdress");
                        houseButton2 = new TextButton("Wear a dress with flowers on", skin); // Use the initialized skin
                        buttonSound.play(0.1f);
                    }
                });
            }
            if (this.player.getInventory().contains("pinkdress")) {
                houseStage.addActor(houseButton3);
                houseButton3.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        player.setSkin("pinkdress");
                        houseButton3 = new TextButton("Wear a pink dress", skin); // Use the initialized skin
                        buttonSound.play(0.1f);

                    }
                });
            }
            if (this.player.getInventory().contains("purplebrown")) {
                houseStage.addActor(houseButton4);
                houseButton4.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        player.setSkin("purplebrown");
                        houseButton4 = new TextButton("Wear a purple and brown skin", skin); // Use the initialized skin
                        buttonSound.play(0.1f);

                    }
                });
            }
            if (this.player.getInventory().contains("redblue")) {
                houseStage.addActor(houseButton5);
                houseButton5.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        player.setSkin("redblue");
                        houseButton5 = new TextButton("Wear a red and blue skin", skin); // Use the initialized skin
                        buttonSound.play(0.1f);
                    }
                });
            }
        }

        if (stablesIsDrawn) {
            Gdx.input.setInputProcessor(stableStage);
            stableButton.setPosition(200, (Gdx.graphics.getHeight() - 50));
            stableButton2.setPosition(200, (Gdx.graphics.getHeight() - 100));
            stableButton3.setPosition(200, (Gdx.graphics.getHeight() - 150));
            stableStage.addActor(moneyUI);

            if (this.player.getInventory().contains("svein")) {
                stableStage.addActor(stableButton);
                stableButton.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Constants.CHARACTERS_ATLAS_PATH = "Hester/hest.txt";
                        stableButton = new TextButton("Use Svein the horse", skin); // Use the initialized skin
                        buttonSound.play(0.1f);
                    }
                });
            }
            if (this.player.getInventory().contains("tjukken")) {
                stableStage.addActor(stableButton2);
                stableButton2.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Constants.CHARACTERS_ATLAS_PATH = "Hester/tjukken.txt";
                        stableButton2 = new TextButton("Use tjukken", skin); // Use the initialized skin
                        buttonSound.play(0.1f);
                    }
                });
            }
            if (this.player.getInventory().contains("superhorse")) {
                stableStage.addActor(stableButton3);
                stableButton3.addCaptureListener(new ClickListener() {
                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        Constants.CHARACTERS_ATLAS_PATH = "Hester/superhest.txt";
                        stableButton3 = new TextButton("Use the superhorse", skin); // Use the initialized skin
                        buttonSound.play(0.1f);
                    }
                });
            }
        }

        goToMenu = Gdx.input.isKeyPressed(Input.Keys.ESCAPE);

        if (player.getyPos() == Gdx.graphics.getHeight() - (int) player.getVarSize()) {
            obsacleStage.draw();
            obsacleStage.addActor(moneyUI);
            Gdx.input.setInputProcessor(obsacleStage);
            obstacleCourseButton.addCaptureListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    goToSideScroller = true;
                    obstacleCourseButton = new TextButton("Go to obstacle course", skin);
                    buttonSound.play(0.1f);
                }
            });
        }
    }

    /**
     * Keeps the player from going outside the screen
     */
    public void PleaseKeepPlayerInTheMap() {
        if (player.getxPos() < 0) {
            player.setxPos(0);
        }
        if (player.getxPos() > (WIDTH - (int) player.getVarSize())) {
            player.setxPos(WIDTH - (int) player.getVarSize());
        }
        if (player.getyPos() < 0) {
            player.setyPos(0);
        }
        if (player.getyPos() > (HEIGHT - (int) player.getVarSize())) {
            player.setyPos(HEIGHT - (int) player.getVarSize());
        }

    }

    private Animation currentAnimation() {
        switch (this.direction) {
            case LEFT:
                if (player.getSkin().equals("default")) {
                    return this.defaultWalkLeft;
                } else if (player.getSkin().equals("flowerdress")) {
                    return this.flowerdressWalkLeft;
                } else if (player.getSkin().equals("pinkdress")) {
                    return this.pinkdressWalkLeft;
                } else if (player.getSkin().equals("purplebrown")) {
                    return this.purplebrownWalkLeft;
                } else if (player.getSkin().equals("redblue")) {
                    return this.redblueWalkLeft;
                }
                break;
            case RIGHT:
                if (player.getSkin().equals("default")) {
                    return this.defaultWalkRight;
                } else if (player.getSkin().equals("flowerdress")) {
                    return this.flowerdressWalkRight;
                } else if (player.getSkin().equals("pinkdress")) {
                    return this.pinkdressWalkRight;
                } else if (player.getSkin().equals("purplebrown")) {
                    return this.purplebrownWalkRight;
                } else if (player.getSkin().equals("redblue")) {
                    return this.redblueWalkRight;
                }
                break;
            case UP:
                if (player.getSkin().equals("default")) {
                    return this.defaultWalkUp;
                } else if (player.getSkin().equals("flowerdress")) {
                    return this.flowerdressWalkUp;
                } else if (player.getSkin().equals("pinkdress")) {
                    return this.pinkdressWalkUp;
                } else if (player.getSkin().equals("purplebrown")) {
                    return this.purplebrownWalkUp;
                } else if (player.getSkin().equals("redblue")) {
                    return this.redblueWalkUp;
                }
                break;
            case DOWN:
                if (player.getSkin().equals("default")) {
                    return this.defaultWalkDown;
                } else if (player.getSkin().equals("flowerdress")) {
                    return this.flowerdressWalkDown;
                } else if (player.getSkin().equals("pinkdress")) {
                    return this.pinkdressWalkDown;
                } else if (player.getSkin().equals("purplebrown")) {
                    return this.purplebrownWalkDown;
                } else if (player.getSkin().equals("redblue")) {
                    return this.redblueWalkDown;
                }
                break;
            case NONE:
                if (player.getSkin().equals("default")) {
                    return this.defaultWalkStill;
                } else if (player.getSkin().equals("flowerdress")) {
                    return this.flowerdressWalkStill;
                } else if (player.getSkin().equals("pinkdress")) {
                    return this.pinkdressWalkStill;
                } else if (player.getSkin().equals("purplebrown")) {
                    return this.purplebrownWalkStill;
                } else if (player.getSkin().equals("redblue")) {
                    return this.redblueWalkStill;
                }
                break;
        }
        return null;
    }

    public Boolean GoToMenu() {
        return goToMenu;
    }

    public Player getPlayer() {
        return player;
    }

    public Boolean GoToSideScrollingGame() {
        return goToSideScroller;
    }

    private void createBasicSkin() {
        //Create a font
        bitmapFont = new BitmapFont();
        skin = new Skin();
        skin.add("default", bitmapFont);

        //Create a texture
        pixmap = new Pixmap((int) Gdx.graphics.getWidth() / 4, (int) Gdx.graphics.getHeight() / 10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background", new Texture(pixmap));

        //Create a button style
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.down = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.checked = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable("background", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);
    }

    private Skin creatMoneySkin() {
        Skin moneySkin = this.skin;
        moneySkin.add("background", new Texture(Gdx.files.internal("horsecoin.png")));
        return moneySkin;
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }

    public void setGoToMenuFalse() {
        goToMenu = false;
    }

    public void setGoToSideScrollingGameFalse() {
        goToSideScroller = false;
    }

}

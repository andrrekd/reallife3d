package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.Entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Represents the player
 *
 * @author Andreas
 * @version 0.1
 */
public class Player extends Entity {

    //The size defined by the y-pos
    private float varSize;
    private final int movementSpeed;
    private Inventory inventory;
    private static int money;
    private String skin;

    /**
     * Creates a Player with a give position texture and size
     *
     * @param xPos the xPos of the player
     * @param yPos the yPos of the player
     * @param texture the texture of the player
     * @param size the size of the player when y=0
     */
    public Player(int xPos, int yPos, Texture texture, float size) {
        super(xPos, yPos, texture, size);
        this.movementSpeed = 200;
        //Sets the player startsize (y=0)
        this.varSize = super.getSize();
        this.inventory = new Inventory();
        this.skin = "default";
        this.inventory.addItem(new Item("default", "the default skin"));
        this.inventory.addItem(new Item("svein", "Svein the horse"));
        
    }

    public int getMoney() {
        return Player.money;
    }

    private void setMoney(int money) {
        Player.money = money;
    }
    public void addMoney() {
        Player.money++;
    }
    public void addMoney(int money) {
        Player.money += money;
    }
    public void removeMoney() {
        Player.money--;
    }
    public void removeMoney(int money) {
        Player.money -= money;
    }
    /**
     * Returns the current size of the player Defined by the yPos
     *
     * @return the variable size of the player
     */
    public float getVarSize() {
        return this.varSize;
    }

    public void addItem(Item item) {
        inventory.addItem(item);
    }

    public void removeItem(Item item) {
        inventory.removeItem(item);
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    /**
     * Calculates the players size from the y-pos of the player to give make it
     * look "3D"
     *
     * @return the variable size of the player calculated from the y-pos
     */
    public float calculatePlayerSize() {
        this.varSize = (super.getSize() / (1 + (super.getyPos() * 0.001666666f)));
        return varSize;
    }

    /**
     * Moves the player up with speed movmentSpeed * deltatime
     */
    public void movePlayerUp() {
        super.setyPos((int) (super.getyPos() + this.movementSpeed * Gdx.graphics.getDeltaTime()));
    }

    /**
     * Moves the player down with speed movmentSpeed * deltatime
     */
    public void movePlayerDown() {
        super.setyPos((int) (super.getyPos() - this.movementSpeed * Gdx.graphics.getDeltaTime()));
    }

    /**
     * Moves the player left with speed movmentSpeed * deltatime
     */
    public void movePlayerLeft() {
        super.setxPos((int) (super.getxPos() - this.movementSpeed * Gdx.graphics.getDeltaTime()));
    }

    /**
     * Moves the player Right with speed movmentSpeed * deltatime
     */
    public void movePlayerRight() {
        super.setxPos((int) (super.getxPos() + this.movementSpeed * Gdx.graphics.getDeltaTime()));
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

}

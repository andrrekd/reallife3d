/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

/**
 *
 * @author Andreas
 */
public class Item {

    private String name;
    private String description;
    private Texture texture;

    public Item(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public void addTexture(Texture texture) {
        this.texture = texture;
    }

    public Texture getTexture() {
        return this.texture;

    }
}

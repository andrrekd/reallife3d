package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Represents a entity in the game (player, building, etc.)
 * @author Andreas
 * @version 0.1
 */
public class Entity extends Actor{
    //The x-position of the entity
    private int xPos;
    //The y-position of the entity
    private int yPos;
    //The texture of the entity
    private Texture texture;
    //The size of the entity
    private float size;

    /**
     * Creates a entity with a given position, size and texture
     * @param xPos x-position of the entity
     * @param yPos y-position of the entity
     * @param texture texture of the entity
     * @param size size of the entity
     */
    public Entity(int xPos, int yPos, Texture texture, float size) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.texture = texture;
        this.size = size;
    }

    /**
     * Returns the xPos of the entity
     * @return the xPos of the entity
     */
    public int getxPos() {
        return xPos;
    }
    /**
     * Returns the yPos of the entity
     * @return the yPos of the entity
     */
    public int getyPos() {
        return yPos;
    }
    /**
     * Returns the texture of the entity
     * @return the texture of the entity
     */
    public Texture getTexture() {
        return texture;
    }
    /**
     * Returns the size of the entity
     * @return the size of the entity
     */
    public float getSize() {
        return size;
    }
    /**
     * Sets the x-position of the entity
     * @param xPos the x-Position of the entity
     */
    public void setxPos(int xPos) {
        this.xPos = xPos;
    }
    /**
     * Sets the y-position of the entity
     * @param yPos the y-position of the entity
     */
    public void setyPos(int yPos) {
        this.yPos = yPos;
    }
    /**
     * Sets the texture of the entity
     * @param texture the texture of the entity
     */
    public void setTexture(Texture texture) {
        this.texture = texture;
    }
    /**
     * Sets the size of the entity
     * @param size the size of the entity
     */
    public void setSize(float size) {
        this.size = size;
    }
    
}

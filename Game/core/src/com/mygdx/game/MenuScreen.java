/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


/**
 *
 * @author erikfossum
 */
public class MenuScreen implements Screen{
    
    private Skin skin;
    private BitmapFont bitmapFont;
    private Pixmap pixmap;
    private final Stage stage;
    private boolean continueGame;
    private final TextButton exitGameButton;
    private TextButton continueGameButton;
    private TextButton saveGameButton;
    private final Texture backgroundTexture;
    private final Image bgImage;
    private Sound buttonSound;
    
    
    public MenuScreen(){
        continueGame = false;
        stage = new Stage();
        createBasicSkin();
        continueGameButton = new TextButton("Continue Game", skin); // Use the initialized skin
        exitGameButton = new TextButton("Exit game", skin); // Use the initialized skin
        saveGameButton = new TextButton("Save Game (not implemented)", skin); // Use the initialized skin
        backgroundTexture = new Texture(Gdx.files.internal("field.png"));
        bgImage = new Image(backgroundTexture);
        buttonSound = Gdx.audio.newSound(Gdx.files.internal("button.wav"));
        long id = buttonSound.play(0.1f);
        buttonSound.setVolume(id, 0.1f);
    }
    
    @Override
    public void show() {
        stage.addActor(bgImage);
        continueGameButton.setPosition(Gdx.graphics.getWidth()/2 - Gdx.graphics.getWidth()/8 , (Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/4) );
        stage.addActor(continueGameButton);
        
        exitGameButton.setPosition(Gdx.graphics.getWidth()/2 - Gdx.graphics.getWidth()/8 , (Gdx.graphics.getHeight() - 450f) );
        stage.addActor(exitGameButton);
        
        saveGameButton.setPosition(Gdx.graphics.getWidth()/2 - Gdx.graphics.getWidth()/8 , (Gdx.graphics.getHeight() - Gdx.graphics.getHeight()/2) );
        stage.addActor(saveGameButton);
        Gdx.input.setInputProcessor(stage);// Make the stage consume events
    }

    @Override
    public void render(float delta) {

        // Clear the screen
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        stage.act();
        stage.draw();
        
        continueGameButton.addCaptureListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                continueGame = true;
                continueGameButton = new TextButton("Continue Game", skin);
                buttonSound.play(0.1f);
            }   
        });
        
        exitGameButton.addCaptureListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                buttonSound.play(0.1f);
                Gdx.app.exit();
            }   
        });
        
        saveGameButton.addCaptureListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                buttonSound.play(0.1f);
                saveGameButton = new TextButton("Save Game (not implemented)", skin);
            }   
        });
    }
    
    
    public boolean isContinued() {
        return this.continueGame;
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
        
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        
    }
    
    private void createBasicSkin(){
        //Create a font
        bitmapFont = new BitmapFont();
        skin = new Skin();
        skin.add("default", bitmapFont);

        //Create a texture
        pixmap = new Pixmap((int)Gdx.graphics.getWidth()/4,(int)Gdx.graphics.getHeight()/10, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background",new Texture(pixmap));

        //Create a button style
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.down = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.checked = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable("background", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont("default");
        skin.add("default", textButtonStyle);
    }
    
    public void setFalse() {
        continueGame = false;
    }
    
}

 


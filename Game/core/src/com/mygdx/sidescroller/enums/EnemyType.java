/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.enums;

import com.mygdx.sidescroller.utils.Constants;
/**
 *
 * @author Patrick
 */
public enum EnemyType {
    
    OBSTACLE_SMALL(1f, 1f, Constants.OBSTACLE_X, Constants.SHORT_OBSTACLE_Y, Constants.OBSTACLE_DENSITY,
            Constants.SMALL_OBSTACLE),
    OBSTACLE_WIDE(1.8f, 1f, Constants.OBSTACLE_X, Constants.SHORT_OBSTACLE_Y, Constants.OBSTACLE_DENSITY,
            Constants.WIDE_OBSTACLE),
    OBSTACLE_HIGH(1f, 1.8f, Constants.OBSTACLE_X, Constants.LONG_OBSTACLE_Y, Constants.OBSTACLE_DENSITY, 
            Constants.LONG_OBSTACLE);
    
    private float width;
    private float height;
    private float x;
    private float y;
    private float density;
    private String regions;
    
    EnemyType(float width, float height, float x, float y, float density, String regions) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.density = density;
        this.regions = regions;
    }
    
    public float getWidth() {
        return width;
    }
    
    public float getHeight() {
        return height;
    }
    
    public float getX() {
        return x;
    }
    
    public float getY() {
        return y;
    }
    
    public float getDensity() {
        return density;
    }
    
    public String getRegions() {
        return regions;
    }
}

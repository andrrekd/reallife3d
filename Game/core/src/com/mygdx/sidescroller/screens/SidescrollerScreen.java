/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Player;
import com.mygdx.sidescroller.stages.GameStage;

public class SidescrollerScreen implements Screen {

    private GameStage stage;
    
    private boolean goToGameScreen;
    private boolean continueGame;
    private Skin skin;
    private BitmapFont bitmapFont;
    private Pixmap pixmap;
    private final TextButton restartGameButton;
    private TextButton goBackButton;
    private Sound loseSound;
    
    public SidescrollerScreen() {
        stage = new GameStage();
        goToGameScreen = false;
        loseSound = Gdx.audio.newSound(Gdx.files.internal("lose.wav"));
        
        createBasicSkin();
        restartGameButton = new TextButton("Restart Game", skin); // Use the initialized skin
        goBackButton = new TextButton("Go Back", skin); // Use the initialized skin
        
        restartGameButton.setPosition(Gdx.graphics.getWidth() -pixmap.getWidth()/2 , Gdx.graphics.getHeight());
        
        goBackButton.setPosition(Gdx.graphics.getWidth() - pixmap.getWidth()/2 , Gdx.graphics.getHeight()/2);
    }

    @Override
    public void render(float delta) {
        //Clear the screen
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //Update the stage
        stage.draw();
        stage.act(delta);
        if (stage.isPlayerOutOfBOunds() == true) {
            goToGameScreen = true;
            loseSound.play();
        }
    }
    
    public Boolean goToGameScreen() {
        return goToGameScreen;
    }
    
    public void setGoToGameScreenFalse() {
        goToGameScreen = false;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);// Make the stage consume events
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
    
     private void createBasicSkin(){
        //Create a font
        bitmapFont = new BitmapFont();
        skin = new Skin();
        skin.add("default", bitmapFont);

        //Create a texture
        pixmap = new Pixmap((int)Gdx.graphics.getWidth()/2,(int)Gdx.graphics.getHeight()/5, Pixmap.Format.RGB888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("background",new Texture(pixmap));

        //Create a button style
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("background", Color.GRAY);
        textButtonStyle.down = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.checked = skin.newDrawable("background", Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable("background", Color.PINK);
        textButtonStyle.font = skin.getFont("default");
        
        skin.add("default", textButtonStyle);
    }
     
    public GameStage getStage() {
        return stage;
    }

}
package com.mygdx.sidescroller;

import com.badlogic.gdx.Game;
import com.mygdx.sidescroller.screens.SidescrollerScreen;

public class sidescroller extends Game {
	
	@Override
	public void create () {
            setScreen(new SidescrollerScreen());
        }
}

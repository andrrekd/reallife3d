/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.sidescroller.box2d.EnemyUserData;
import com.mygdx.sidescroller.utils.Constants;
/**
 *
 * @author Patrick
 */
public class Obstacle extends GameActor {
    
    private Animation animation;
    private float stateTime;
    
    public Obstacle(Body body) {
        super(body);
        TextureAtlas textureAtlas = new TextureAtlas(Constants.HURDLE_ATLAS_PATH);
        TextureRegion[] runningFrames = new TextureRegion[getUserData().getTextureRegions().length()];
        for (int i = 0; i < getUserData().getTextureRegions().length(); i++) {
            String path = getUserData().getTextureRegions();
            runningFrames[i] = textureAtlas.findRegion(path);
        }
        animation = new Animation(0.1f, runningFrames);
        stateTime = 0f;
    }
    
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        stateTime += Gdx.graphics.getDeltaTime();
        batch.draw((TextureRegion) animation.getKeyFrame(stateTime, true), (screenRectangle.x - (screenRectangle.width * 0.1f)),
                screenRectangle.y, screenRectangle.width * 1.2f, screenRectangle.height * 1.1f);
    }
    
    @Override
    public EnemyUserData getUserData() {
        return (EnemyUserData) userData;
    }
    
    @Override
    public void act(float delta) {
        super.act(delta);
        body.setLinearVelocity(getUserData().getLinearVelocity());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.stages;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.mygdx.sidescroller.actors.*;
import com.mygdx.sidescroller.utils.BodyUtils;
import com.mygdx.sidescroller.utils.Constants;
import com.mygdx.sidescroller.utils.WorldUtils;

public class GameStage extends Stage implements ContactListener {

    // This will be our viewport measurements while working with the debug renderer
    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    private static final int VIEWPORT_HEIGHT = Constants.APP_HEIGHT;
    
    private boolean isPlayerOutOfBounds;

    private World world;
    private Ground ground;
    private Runner runner;
    private Obstacle obstacle;

    private final float TIME_STEP = 1 / 300f;
    private float accumulator = 0f;

    private OrthographicCamera camera;
    private Box2DDebugRenderer renderer;
    private Rectangle screenRightSide;
    private Vector3 touchPoint;   
    
    private int numberOfObstacles;
    Sound rewardSound;
    
   
    public GameStage() {
        super(new ScalingViewport(Scaling.stretch, VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
                new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));
        setUpWorld();
        setUpCamera();
        setUpTouchControlAreas();
        renderer = new Box2DDebugRenderer();
        isPlayerOutOfBounds = false;
        numberOfObstacles = 0;
        rewardSound = Gdx.audio.newSound(Gdx.files.internal("reward.wav"));
    }
    
    private void setUpWorld() {
        world = WorldUtils.createWorld();
        world.setContactListener(this);
        setUpBackground();
        setUpGround();
        setUpRunner();
        createObstacle();
    }
    
    private void setUpBackground() {
        addActor(new Background());
    }
    
    private void setUpGround() {
        ground = new Ground(WorldUtils.createGround(world));
        addActor(ground);
    }
    
    public void setUpRunner() {
        runner = new Runner(WorldUtils.createRunner(world));
        addActor(runner);
    }

    private void setUpCamera() {
        camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0f);
        camera.update();
    }
    
    private void setUpTouchControlAreas() {
        touchPoint = new Vector3();
        screenRightSide = new Rectangle(getCamera().viewportWidth / 2, 0, getCamera().viewportWidth / 2,
                getCamera().viewportHeight);
        Gdx.input.setInputProcessor(this);
    }

 @Override
    public void act(float delta) {
        super.act(delta);

        Array<Body> bodies = new Array<Body>(world.getBodyCount());
        world.getBodies(bodies);

        for (Body body : bodies) {
            update(body);
        }

        // Fixed timestep
        accumulator += delta;

        while (accumulator >= delta) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }
    }
    
    private void update(Body body) {
        if (!BodyUtils.bodyInBounds(body)){
            if (BodyUtils.bodyIsObstacle(body) && !runner.isHit()) {
                createObstacle();
                if(numberOfObstacles >= 0) {
                    rewardSound.play();
                }
                numberOfObstacles++;
            }
            world.destroyBody(body);
        } 
        
        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            runner.jump();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
            runner.jump();
        }
        
    }
    
    /**
     * get the number of obstacles the player has cleard
     * @return number of obstacles
     */
    public int getNumberOfObstacles() {
        return numberOfObstacles;
    }
    
    /**
     * sets the number back of obstacles back to starting point.
     * puts it at -1 because the player will automatic get 1 obstacle the
     * second time it enters the obstaclecourse
     */
    public void setNumberOfObstaclesToZero() {
        numberOfObstacles = -1;
    }
    
    public Boolean isPlayerOutOfBOunds() {
        isPlayerOutOfBounds = RunnerPos() < -1f;
        return isPlayerOutOfBounds;
    }
    
    public void removeObsatcle() {
       for (Object actor : getActors()) {
           if (actor instanceof Obstacle) {
               ((Obstacle) actor).remove();
           }
       }
    }
    
    private void createObstacle() {
    obstacle = new Obstacle(WorldUtils.createObsacle(world));
    addActor(obstacle);
}

    @Override
    public void draw() {
        super.draw();
        renderer.render(world, camera.combined);
    }
    
    public void render() {

    } 
            
    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        translateScreenToWorldCoordinates(x, y);
        if (rightSideTouched(touchPoint.x, touchPoint.y)){
            runner.jump();
        }
        return super.touchDown(x, y, pointer, button);
    }
    
    @Override
    public boolean keyDown(int SPACE) {
        return false;
    }
    
    private boolean rightSideTouched(float x, float y) {
        return screenRightSide.contains(x,y);
    }
    
    private void translateScreenToWorldCoordinates(int x, int y) {
        getCamera().unproject(touchPoint.set(x, y, 0));
    }
    
    @Override
    public void beginContact(Contact contact) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();
        
        if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsRunner(b))) {
            runner.landed();
        }
    }
    
    @Override
    public void endContact(Contact contact) {
        
    }
 
    @Override
    public void preSolve(Contact contact, Manifold oldManifold){
        
    }
    
    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
    
    public float RunnerPos() {
        return runner.getXPos();
    }
    
    
}
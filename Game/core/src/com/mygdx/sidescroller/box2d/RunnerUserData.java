/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.box2d;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.sidescroller.enums.UserDataType;
import com.mygdx.sidescroller.utils.Constants;
/**
 *
 * @author Patrick
 */
public class RunnerUserData extends UserData {
    
    private Vector2 jumpingLinearImpulse;
    
    public RunnerUserData(float width, float height) {
        super(width, height);
        jumpingLinearImpulse = Constants.RUNNER_JUMPING_LINEAR_IMPULSE;
        userDataType = UserDataType.RUNNER;
}

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }

    public void setJumpingLinearImpulse(Vector2 jumpingLinearImpulse) {
        this.jumpingLinearImpulse = jumpingLinearImpulse;
    }
    
    public float getHitAngularImpulse() {
        return Constants.RUNNE_HIT_ANGULAR_IMPULSE; 
    }
}

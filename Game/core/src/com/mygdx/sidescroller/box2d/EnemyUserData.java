/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.box2d;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.sidescroller.utils.Constants;
import com.mygdx.sidescroller.enums.UserDataType;
/**
 *
 * @author Patrick
 */
public class EnemyUserData extends UserData {
    
    private Vector2 linearVelocity;
    private String textureRegions;
    
    public EnemyUserData(float width, float height, String textureRegions) {
        super(width, height);
        userDataType = UserDataType.OBSTACLE;
        linearVelocity = Constants.OBSTACLE_LINEAR_VELOCITY;
        this.textureRegions = textureRegions;
    }
    
    public void setLinearVelocity(Vector2 linearVelocity) {
        this.linearVelocity = linearVelocity;
    }
    
    public Vector2 getLinearVelocity() {
        return linearVelocity;
    }
    
    public String getTextureRegions() {
        return textureRegions;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.box2d;

import com.mygdx.sidescroller.enums.UserDataType;

/**
 *
 * @author Patrick
 */
public class GroundUserData extends UserData {
    
    public GroundUserData(float width, float height) {
        super(width, height);
        userDataType = UserDataType.GROUND;
    }
}

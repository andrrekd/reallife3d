/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.utils;

import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author Patrick
 */
public class Constants {
    
    public static final int APP_WIDTH = 800 * 2;
    public static final int APP_HEIGHT = 480 * 2;
    public static final int WORLD_TO_SCREEN = 32 * 2;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, -10);

    public static final float GROUND_X = 0;
    public static final float GROUND_Y = 0;
    public static final float GROUND_WIDTH = 45f;
    public static final float GROUND_HEIGHT = 2f;
    public static final float GROUND_DENSITY = 0f;

    public static final float RUNNER_X = 2;
    public static final float RUNNER_Y = GROUND_Y + GROUND_HEIGHT;
    public static final float RUNNER_WIDTH = 3f;
    public static final float RUNNER_HEIGHT = 1.5f;
    public static final float RUNNER_DENSITY = 0.5f;

    public static final float RUNNER_GRAVITY_SCALE = 3f;
    public static final Vector2 RUNNER_JUMPING_LINEAR_IMPULSE = new Vector2(0,32f);
    public static final float RUNNE_HIT_ANGULAR_IMPULSE = 10f;

    public static final float OBSTACLE_X = 25f;
    public static final float OBSTACLE_DENSITY = RUNNER_DENSITY;
    public static final float SHORT_OBSTACLE_Y = 1.5f;
    public static final float LONG_OBSTACLE_Y = 2f;
    public static final Vector2 OBSTACLE_LINEAR_VELOCITY = new Vector2(-10f, 0);

    public static final String BACKGROUND_IMAGE_PATH = "background2.png";
    public static final String GROUND_IMAGE_PATH = "ground.png";
    public static String CHARACTERS_ATLAS_PATH = "Hester/hest.txt";
    public static final String[] RUNNER_RUNNING_REGION_NAMES = new String[]{"hest-walk1", "hest-walk2"};
    public static final String RUNNER_JUMPING_REGION_NAME = "hest-jump";

    public static final String HURDLE_ATLAS_PATH = "Hurdle/hurdle.txt";
    public static final String SMALL_OBSTACLE = "1x1hurdle";
    public static final String WIDE_OBSTACLE = "1x2hurdle";
    public static final String LONG_OBSTACLE = "2x1hurdle";

    private Constants(){
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.utils;

import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.sidescroller.box2d.UserData;
import com.mygdx.sidescroller.enums.UserDataType;
/**
 *
 * @author Patrick
 */
public class BodyUtils {
    
    public static boolean bodyInBounds(Body body) {
        UserData userData = (UserData) body.getUserData();
        
        switch (userData.getUserDataType()) {
            case RUNNER:
            case OBSTACLE:
                return  body.getPosition().x + userData.getWidth() / 2 > 0;
        }
        return true;
    }
    
    public static boolean bodyIsObstacle(Body body) {
        UserData userData = (UserData) body.getUserData();
        
        return userData != null && userData.getUserDataType() == UserDataType.OBSTACLE;
    }
    
    public static boolean bodyIsRunner(Body body) {
        UserData userData = (UserData) body.getUserData();
        
        return userData != null && userData.getUserDataType() == UserDataType.RUNNER;
    }
    
    public static boolean bodyIsGround(Body body) {
        UserData userData = (UserData) body.getUserData();
        
        return userData != null && userData.getUserDataType() == UserDataType.GROUND;
    }
}

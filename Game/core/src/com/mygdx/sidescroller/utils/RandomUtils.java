/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mygdx.sidescroller.utils;

import com.mygdx.sidescroller.enums.EnemyType;

import java.util.Random;
/**
 *
 * @author Patrick
 */
public class RandomUtils {
    
    public static EnemyType getRandomEnemyType() {
        RandomEnum<EnemyType> randomEnum = new RandomEnum<EnemyType>(EnemyType.class);
        return randomEnum.random();
    }
    
    private static class RandomEnum<E extends Enum> {
        
        private static final Random RND = new Random();
        private final E[] values;
        
        public RandomEnum(Class<E> token) {
            values = token.getEnumConstants();
        }
        public E random () {
            return values[RND.nextInt(values.length)];
            }
        }
    }

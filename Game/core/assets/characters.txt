characters.png
size: 200, 448
format: RGBA8888
filter: Linear,Linear
repeat: none
1x1hurdle
  rotate: false
  xy: 0, 0
  size: 100, 100
  orig: 100, 100
  offset: 0, 0
  index: -1
1x2hurdle
  rotate: false
  xy: 100, 0
  size: 100, 200
  orig: 100, 200
  offset: 0, 0
  index: -1
2x1hurdle
  rotate: false
  xy: 0, 200
  size: 200, 100
  orig: 200, 100
  offset: 0, 0
  index: -1
horse
  rotate: false
  xy: 0, 300
  size: 96, 74
  orig: 96, 74
  offset: 0, 0
  index: -1
horse_jump
  rotate: false
  xy: 96, 300
  size: 96, 74
  orig: 96, 74
  offset: 0, 0
  index: -1
horse_run
  rotate: false
  xy: 0, 374
  size: 96, 74
  orig: 96, 74
  offset: 0, 0
  index: -1
strechhorse
  rotate: false
  xy: 96, 374
  size: 96, 67
  orig: 96, 67
  offset: 0, 0
  index: -1

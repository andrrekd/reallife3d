front.png
size: 26, 126
format: RGBA8888
filter: Linear,Linear
repeat: none
char-front-still-gb
  rotate: false
  xy: 0, 0
  size: 26, 42
  orig: 26, 42
  offset: 0, 0
  index: -1
char-front-walk1-gb
  rotate: false
  xy: 0, 42
  size: 26, 42
  orig: 26, 42
  offset: 0, 0
  index: -1
char-front-walk2-gb
  rotate: false
  xy: 0, 84
  size: 26, 42
  orig: 26, 42
  offset: 0, 0
  index: -1

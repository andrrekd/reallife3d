<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.6.1</string>
        <key>fileName</key>
        <string>C:/Users/Patrick/Documents/Dataingeniør - NTNU Ålesund/Semester 4/Systemutvikling og modellering/Real Life 3D/Art Assets/Sprite Sheet/V3/char.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>libgdx</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../Character/Default Skin/char-back-still-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-back-walk1-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-back-walk2-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-front-still-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-front-walk1-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-front-walk2-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-left-still-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-left-walk1-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-left-walk2-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-right-still-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-right-walk1-gb.png</key>
            <key type="filename">../../Character/Default Skin/char-right-walk2-gb.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-back-still-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-back-walk1-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-back-walk2-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-front-still-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-front-walk1-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-front-walk2-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-left-still-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-left-walk1-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-left-walk2-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-right-still-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-right-walk1-fldress.png</key>
            <key type="filename">../../Character/Flower Dress Skin/char-right-walk2-fldress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-back-still-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-back-walk1-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-back-walk2-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-front-still-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-front-walk1-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-front-walk2-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-left-still-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-left-walk1-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-left-walk2-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-right-still-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-right-walk1-pinkdress.png</key>
            <key type="filename">../../Character/Pink Dress Skin/char-right-walk2-pinkdress.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-back-still-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-back-walk1-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-back-walk2-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-front-still-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-front-walk1-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-front-walk2-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-left-still-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-left-walk1-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-left-walk2-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-right-still-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-right-walk1-pb.png</key>
            <key type="filename">../../Character/Purple-brown Skin/char-right-walk2-pb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-back-still-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-back-walk1-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-back-walk2-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-front-still-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-front-walk1-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-front-walk2-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-left-still-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-left-walk1-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-left-walk2-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-right-still-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-right-walk1-rb.png</key>
            <key type="filename">../../Character/Red-blue Skin/char-right-walk2-rb.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,11,13,21</rect>
                <key>scale9Paddings</key>
                <rect>7,11,13,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../Character/Flower Dress Skin/char-left-still-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-left-walk1-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-left-walk2-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-right-still-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-right-walk1-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-right-walk2-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-back-still-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-back-walk1-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-back-walk2-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-front-still-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-front-walk1-fldress.png</filename>
            <filename>../../Character/Flower Dress Skin/char-front-walk2-fldress.png</filename>
            <filename>../../Character/Default Skin/char-front-walk2-gb.png</filename>
            <filename>../../Character/Default Skin/char-left-still-gb.png</filename>
            <filename>../../Character/Default Skin/char-left-walk1-gb.png</filename>
            <filename>../../Character/Default Skin/char-left-walk2-gb.png</filename>
            <filename>../../Character/Default Skin/char-right-still-gb.png</filename>
            <filename>../../Character/Default Skin/char-right-walk1-gb.png</filename>
            <filename>../../Character/Default Skin/char-right-walk2-gb.png</filename>
            <filename>../../Character/Default Skin/char-back-still-gb.png</filename>
            <filename>../../Character/Default Skin/char-back-walk1-gb.png</filename>
            <filename>../../Character/Default Skin/char-back-walk2-gb.png</filename>
            <filename>../../Character/Default Skin/char-front-still-gb.png</filename>
            <filename>../../Character/Default Skin/char-front-walk1-gb.png</filename>
            <filename>../../Character/Pink Dress Skin/char-front-walk2-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-left-still-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-left-walk1-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-left-walk2-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-right-still-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-right-walk1-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-right-walk2-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-back-still-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-back-walk1-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-back-walk2-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-front-still-pinkdress.png</filename>
            <filename>../../Character/Pink Dress Skin/char-front-walk1-pinkdress.png</filename>
            <filename>../../Character/Purple-brown Skin/char-front-walk2-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-left-still-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-left-walk1-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-left-walk2-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-right-still-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-right-walk1-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-right-walk2-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-back-still-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-back-walk1-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-back-walk2-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-front-still-pb.png</filename>
            <filename>../../Character/Purple-brown Skin/char-front-walk1-pb.png</filename>
            <filename>../../Character/Red-blue Skin/char-front-walk2-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-left-still-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-left-walk1-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-left-walk2-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-right-still-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-right-walk1-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-right-walk2-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-back-still-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-back-walk1-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-back-walk2-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-front-still-rb.png</filename>
            <filename>../../Character/Red-blue Skin/char-front-walk1-rb.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>

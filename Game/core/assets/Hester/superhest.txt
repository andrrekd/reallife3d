superhest.png
size: 96, 222
format: RGBA8888
filter: Linear,Linear
repeat: none
hest-jump
  rotate: false
  xy: 0, 0
  size: 96, 74
  orig: 96, 74
  offset: 0, 0
  index: -1
hest-walk1
  rotate: false
  xy: 0, 74
  size: 96, 74
  orig: 96, 74
  offset: 0, 0
  index: -1
hest-walk2
  rotate: false
  xy: 0, 148
  size: 96, 74
  orig: 96, 74
  offset: 0, 0
  index: -1
